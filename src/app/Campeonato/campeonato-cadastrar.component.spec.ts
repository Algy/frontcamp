import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampeonatoCadastrarComponent } from './campeonato-cadastrar.component';

describe('CampeonatoCadastrarComponent', () => {
  let component: CampeonatoCadastrarComponent;
  let fixture: ComponentFixture<CampeonatoCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampeonatoCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampeonatoCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
