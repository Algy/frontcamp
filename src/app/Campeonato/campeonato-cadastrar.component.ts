// import { Component, OnInit, Input } from '@angular/core';
// import { Location } from '@angular/common';

// @Component({
//   selector: 'app-campeonato-cadastrar',
//   templateUrl: './campeonato-cadastrar.component.html',
//   styleUrls: ['./campeonato-cadastrar.component.css']
// })
// export class CampeonatoCadastrarComponent implements OnInit {

//   @Input() campeonato: Campeonato;
//   campeonatos: Campeonato[];
//   constructor(private location: Location) { }

//   ngOnInit() {
//     this.campeonato = new Campeonato();
//     this.getCampeonatos();
//   }
//   getCampeonatos(): void{
//     this.campeonatos = [
//       { id: 11, nome: 'Mr. Nice', descricao: "1" },
//       { id: 12, nome: 'Narco', descricao: "2" },
//       { id: 13, nome: 'Bombasto', descricao: "3" },
//       { id: 14, nome: 'Celeritas', descricao: "4" },
//       { id: 15, nome: 'Magneta', descricao: "5" },
//       { id: 16, nome: 'RubberMan', descricao: "Gato" },
//       { id: 17, nome: 'Dynama', descricao: "Gato" },
//       { id: 18, nome: 'Dr IQ', descricao: "Gato" },
//       { id: 19, nome: 'Magma', descricao: "Gato"},
//       { id: 20, nome: 'Tornado', descricao: "Gato"}
//     ]
//   }

//   onSubmit(): void{
//     console.log(this.campeonato);
//   }
//   goBack(): void {
//     this.location.back();
//   }
// }
