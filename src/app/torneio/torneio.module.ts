import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TorneioComponent } from './torneio.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TorneioComponent]
})
export class TorneioModule { }
