import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 11, name: 'DOTA2' },
      { id: 12, name: 'CS:GO' },
      { id: 13, name: 'League of Legends' },
      { id: 14, name: 'BattleField' },
      { id: 15, name: 'Pubg' },
      { id: 16, name: 'Free Fire' },
      { id: 17, name: 'Heartstone' },
      { id: 18, name: 'Overwatch' },
      { id: 19, name: 'Fortnite' },
      { id: 20, name: 'Call of Dute' }
    ];
    return {heroes};
  }
}
